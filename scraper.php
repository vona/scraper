<?php
class scraper {
	public $options = array(
		"proxy" => "",
		"proxyCrawlera" => false,
		"proxyTor" => false,
		"proxyType" => CURLPROXY_SOCKS5,		
		"proxyAuth" => "",
		"timeout"  => 30,
		"method"   => "get",
		"postvars" => "",
		"preheaders"   => array(),
		"headers"   => array(),
		"cachingDir" => "",
		"onlyweb" => false,
		"onlyfile" => false,
		"cookieName" => "cookie",
		"manualCookie" => "",
		"zeroResultTimes" => 3,
		"banText" => "you have been blocked",
		"unlinkBytes" => 0,
		"follow" => true,
		"debug" => true,
		"banDelay" => 2,
		"zeroResultDelay" => 3,
		"zeroResultPass" => false,
		"userpass" => "",
	);
	public function scrap() {
		unset($this->result);
		unset($this->status);
		if (!empty($this->options['cachingDir'])) {
			if (!is_dir($this->options['cachingDir'])) mkdir($this->options['cachingDir']);
			$fpath = $this->options['cachingDir'].(isset($this->id)?$this->id:md5($this->url.$this->options['postvars'])).".html";
			
			if (file_exists($fpath)) {
				if (filesize($fpath)<=$this->options['unlinkBytes']) {
					unlink($fpath);
					goto rescrap;
				}
				if ($this->options['onlyweb']) {
					$this->status['ok'] = false;
					$this->html = '';
					$this->result[0] = array();
					return;
				}
				$this->html = file_get_contents($fpath);
				$this->status['loadFile'] = $fpath; 
				$this->status['ok'] = true;
				
				if (!is_array($this->pattern)) {
					if (!empty($this->pattern)) {
						preg_match_all($this->pattern,$this->html,$this->result);
					} else {
						$this->result = array();
					}
				} else {
					foreach ($this->pattern as $p) {
						preg_match_all($p,$this->html,$this->result[]);
					}
				}
				
				
				return;
			}
		}
		$zeroResultTimes = $this->options['zeroResultTimes'];
		rescrap:;
		if ($this->options['onlyfile']) { return; }
			
		if (isset($fpath)) file_put_contents($fpath,"0");
		$this->html = $this->gethtml();
		if (strlen($this->html)>0) {
			if (strpos($this->html,$this->options['banText'])===false) {
				if (isset($fpath)) file_put_contents($fpath,$this->html);
			} else {
				banned:;
				if ($this->options['proxy']) {
					$this->debug($this->options['proxy'] = $this->getProxy());
				}
				$this->debug("banned, tring to rescrap");
				$this->debug("waiting {$this->options['banDelay']} secs");
				sleep($this->options['banDelay']);
				goto rescrap;
			}
		} else {
			if ($this->options['zeroResultPass']) {
				return;
			}			
			$zeroResultTimes--;
			$this->debug("zero result, waiting {$this->options['zeroResultDelay']} secs");
			sleep($this->options['zeroResultDelay']);
			if ($zeroResultTimes == 0) {
				$zeroResultTimes = $this->options['zeroResultTimes'];
				if ($this->options['proxy']) {
					$this->debug($this->options['proxy'] = $this->getProxy());
				}
			}
			goto rescrap;
		}
		$this->status['ok'] = true;
		unset($this->status['loadFile']);
		if (!is_array($this->pattern)) {
			if (!empty($this->pattern)) {
				preg_match_all($this->pattern,$this->html,$this->result);
			} else {
				$this->result = array();
			}			
		} else {
			foreach ($this->pattern as $p) {
				preg_match_all($p,$this->html,$this->result[]);
			}
		}
	}
	private function gethtml() {
		$ch = curl_init();
		$this->options['headers'] = array();
		if (count($this->options['preheaders'])>0) $this->options['headers'] = $this->options['preheaders'];

		if ($this->options['method'] == "post") {
			curl_setopt($ch,CURLOPT_POST, 1); 
			curl_setopt($ch,CURLOPT_POSTFIELDS, $this->options['postvars']);
			
			if (!preg_grep("#^Content-Type.*#si", $this->options['headers'])) {
				$this->options['headers'][] = 'Content-Type: application/x-www-form-urlencoded';

			}
			
			if (!$this->options['follow']) {
				if (!preg_grep("#^Content-Length.*#si", $this->options['headers'])) {	
					$this->options['headers'][] = 'Content-Length: '.strlen($this->options['postvars']);	
				}
			}
		}
		if (count((array)$this->options['headers'])>0) {
			//curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->options['headers']);
		}
		
		if (!empty($this->options['proxy'])) {
			curl_setopt($ch, CURLOPT_PROXY, $this->options['proxy']);
			curl_setopt($ch, CURLOPT_PROXYTYPE, $this->options['proxyType']);
			if (!empty($this->options['proxyAuth'])) {
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->options['proxyAuth']);
			}
		}
		curl_setopt($ch,CURLOPT_TIMEOUT, $this->options['timeout']);
		curl_setopt($ch,CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36");
		
		if (!empty($this->options['cookieName'])) {
			curl_setopt($ch,CURLOPT_COOKIEFILE, $this->options['cookieName']);	
			curl_setopt($ch,CURLOPT_COOKIEJAR, $this->options['cookieName']);
		}
		if (!empty($this->options['manualCookie'])) {
			curl_setopt($ch, CURLOPT_COOKIE, $this->options['manualCookie']);
		}		
		if (!empty($this->options['userpass'])) {
			curl_setopt($ch, CURLOPT_USERPWD, $this->options['userpass']);
		}		
		
		curl_setopt($ch,CURLOPT_URL, $this->url);
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION, $this->options['follow']);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		$this->status['curl'] = curl_getinfo($ch);
		$this->error = curl_error($ch);
/*
		print_r($this->options['headers']);
		print_r($this->options['postvars']);
		echo curl_error($ch);
		$curlinfo = curl_getinfo($ch);
		print_r($curlinfo);
		print_r($this->options);
*/
		curl_close($ch);
		return $result;
		return $this->trim2($result);
	}
	public function download($url,$out) { 
		$ch = curl_init($url);
		$fp = fopen ($out, 'w+');
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch,CURLOPT_TIMEOUT,50000);
		curl_setopt($ch,CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_FILE, $fp);
		curl_setopt($ch,CURLOPT_PROGRESSFUNCTION, 'downloadProgress');
		curl_setopt($ch,CURLOPT_COOKIEFILE, $this->options['cookieName']);
		curl_setopt($ch,CURLOPT_COOKIEJAR, $this->options['cookieName']);
		if (!empty($this->options['manualCookie'])) {
			curl_setopt($ch, CURLOPT_COOKIE, $this->options['manualCookie']);
		}				
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false);		
		curl_exec($ch);
/*
		print_r($this->options['headers']);
		print_r($this->options['postvars']);
		echo curl_error($ch);
		$curlinfo = curl_getinfo($ch);
		print_r($curlinfo);
		print_r($this->options);
*/		
		curl_close($ch);
		fclose($fp);
	}
	public function trim2($out) {
		$out = str_replace("&nbsp;"," ",$out);
		$out = str_replace("&amp;","&",$out);
		$out = str_replace("\n"," ",$out);
		$out = str_replace("|"," ",$out);
		$out = str_replace("\t"," ",$out);
		$out = preg_replace('/\s+/',' ',$out);	
		return $out;
	}
	private function downloadProgress($dltotal, $dlnow, $ultotal, $ulnow) {
		echo $dltotal/1024;
		echo "kb/";
		echo $dlnow/1024;
		echo "kb\r";
	}
	public function debug($str) {
		if ($this->options['debug']) {
			echo "{$str}\n";
		}
	}
	public function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	public function getIP() {
		return trim(file_get_contents("http://myexternalip.com/raw"));
	}
	public function getTorIP() {
		return trim(shell_exec("curl -s --socks5 127.0.0.1:9050 http://checkip.amazonaws.com/"));
	}
	public function resetTorIP($tor_ip='127.0.0.1', $control_port='9051', $auth_code='naber'){
	    $fp = fsockopen($tor_ip, $control_port, $errno, $errstr, 30);
	    if (!$fp) return "can't connect to the control port";
	     
	    fputs($fp, "AUTHENTICATE \"$auth_code\"\r\n");
	    $response = fread($fp, 1024);
	    list($code, $text) = explode(' ', $response, 2);
	    if ($code != '250') return "authentication failed"; //authentication failed
	     
	    //send the request to for new identity
	    fputs($fp, "signal NEWNYM\r\n");
	    $response = fread($fp, 1024);
	    list($code, $text) = explode(' ', $response, 2);
	    if ($code != '250') return "signal failed"; //signal failed

	    fclose($fp);
	    sleep(5);
	    return true;
	}

	public function getProxy() {	
		if ($this->options['proxyTor']) {
			shell_exec("/etc/init.d/tor restart");
			sleep(3);
			return $this->options['proxy'];
		}
		if ($this->options['proxyCrawlera']) {
			return $this->options['proxy'];			
		}
		$proxies = file(__DIR__."/proxies.txt");
		@array_filter($proxies);
		@shuffle($proxies);
		$rv = trim(@array_shift($proxies));
		file_put_contents(__DIR__."/proxies.txt",implode("",$proxies));
		if (empty($rv)) {
			die("proxy finished");
		}
		return $rv;
	}

	public function htmldecoder($input) {
		return html_entity_decode(preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $input));
	}

	public function captcha($imgPath) {
		$host		= "api.de-captcher.com";	// host to connect to
		$port		= "80";
		$login		= "vona70";		// YOUR LOGIN
		$password	= "99999998";		// YOUR PASSWORD

		$return = poster_curl(
				$host, $port, $login, $password,
				$imgPath,
				NULL,
				ptUNSPECIFIED	// pic type - UNSPECIFIED
			);
		
		if( $return === FALSE ) {
			$this->captchastatus = "We've got an error, please check parameters";
		} else if( is_int( $return ) ) {
			if( $return < 0 ) {
				$this->captchastatus = "Can't process ";
				switch( $return ) {
					case ccERR_GENERAL:
						$this->captchastatus = "general internal error";
						break;
					case ccERR_STATUS:
						$this->captchastatus = "status is not correct";
						break;
					case ccERR_NET_ERROR:
						$this->captchastatus = "network data transfer error";
						break;
					case ccERR_TEXT_SIZE:
						$this->captchastatus = "text is not of an appropriate size";
						break;
					case ccERR_OVERLOAD:
						$this->captchastatus = "server's overloaded";
						break;
					case ccERR_BALANCE:
						$this->captchastatus = "not enough funds to complete the request";
						break;
					case ccERR_TIMEOUT:
						$this->captchastatus = "request timed out";
						break;
					case ccERR_BAD_PARAMS:
						$this->captchastatus = "provided parameters are not good for this function";
						break;
					default:
						$this->captchastatus = "unknown error";
						break;
				}
			} else if( $return < 10 ) {
				$this->captchastatus = "Malformed output data";
			} else {
				$this->captchastatus = "HTTR ERROR $return";
			}
		} else {
			$this->captchastatus = "OK";
			$this->captcharesult = $return;
		}
	}
}
?>